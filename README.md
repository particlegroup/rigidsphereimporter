### Overview ###

A Blender Add-On used to import (x,y,z) position data and create rigid body spheres at each position.

Version 0.1.0

### Setup Instructions ###

Needed: Blender, Python 2.7

### Contact Details ###

* Repo owner or admin
* Other community or team contact